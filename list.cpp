#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node* tmp = new Node(el); //create new node
	tail->next = tmp; //tmp is next of tail
	tail = tmp; //tail point at tmp

	//TO DO!
}
char List::popHead()
{
	char el = head->data;
	Node* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node* a = head; //create new pointer
	while (a->next != tail) { //next if it not point at tail
		a = a->next; //move a to next
	}
	char save = tail->data; //save tail value
	delete tail;
	tail = a;
	// TO DO!
	return save;
	// TO DO!
	return NULL;
}
bool List::search(char el)
{
	Node* b = head; //create new pointer
	while (b != NULL) {
		if (b->data == el) {
			return true;
		}
		b = b->next; //move b to next
	}

	// TO DO! (Function to return True or False depending if a character is in the list.
	return false; //if it not find data untill NULL it will return false
}
void List::reverse()
{
	List reversedata;
	Node* c = head; //create new pointer
	while (c != NULL) {
		reversedata.pushToHead(c->data); //thing that pointed by c will be push to reversedata
		c = c->next;
	}
	// TO DO! (Function is to reverse the order of elements in the list.

	reversedata.print();
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}