#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	mylist.print();
	mylist.pushToTail('c');
	mylist.pushToTail('o');
	mylist.pushToTail('s');
	mylist.pushToTail('h');
	cout << "\n";
	mylist.print();
	cout << "\ntailpop is "<<mylist.popTail();
	cout<<endl;
	if (mylist.search('m')) {
		cout << "FOUND m";
	}
	else {
		cout << "NOT FOUND";
	}
	cout << endl << "reversedata is ";
	mylist.reverse();

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!

}